#!/usr/bin/python
import numpy as np
from os import listdir

PATH = './results/'
RESULTS_FILE = 'table_results.csv'

files = listdir(PATH) # files are ordered as website_rtt_fastopenmode
results_map = dict()

for file_name in files:
	if file_name == '.DS_Store':
		continue

	name, rtt, mode = file_name.split('_')
	# amazon, wsj, nytimes, wikipedia
	if results_map.get(name) is None:
		results_map[name] = dict()
	# rtt - 20, 100, 200
	rtt_map = results_map[name]
	if rtt_map.get(rtt) is None:
		rtt_map[rtt] = dict()
	# mode - on, off
	mode_map = rtt_map[rtt]
	if mode_map.get(mode) is None:
		mode_map[mode] = list()

	with open(str(PATH+file_name), 'r') as f:
		for line in f:
			# assuming page download takes less than an hour
			minutes, seconds = map(float, line.strip().split(':'))
			time = (minutes*60) + seconds
			mode_map[mode].append(time)

# should not be in results folder for future runs
# hard coding keys for maintaining order in resulting csv file
with open(RESULTS_FILE, 'w') as f:
	f.write('Page, RTT, PLT: non-TFO (s), PLT: TFO (s), Improvement\n')
	# for page_key in results_map.keys():
	for page_key in ['amazon', 'nytimes', 'wsj', 'wikipedia']:
		page_map = results_map[page_key]
		# for rtt_key in page_map.keys():
		for rtt_key in ['10ms', '50ms', '100ms']:
			rtt_map = page_map[rtt_key]
			# compute performance and relative improvement
			off = np.mean(rtt_map['off'])
			on = np.mean(rtt_map['on'])
			improvement = ((off-on)/off)*100
			# flush to file
			array = [page_key, rtt_key, str(off), str(on), "%.1f"%improvement]
			line = ','.join(map(str, array)) + '%\n'
			f.write(line)
