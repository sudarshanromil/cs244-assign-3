# 0x200: Accept data-in-SYN w/o any cookie option present.
# 0x001: Enables sending data in the opening SYN on the client w/ MSG_FASTOPEN.
# 0x002: Enables TCP Fast Open on the server side, i.e., allowing data in a SYN packet to be accepted and passed to the application before 3-way hand shake finishes.
# 0x004: Send data in the opening SYN regardless of cookie availability and without a cookie option.
# Net hex value = 0x207
sudo sysctl net.ipv4.tcp_fastopen=519
