#!/usr/bin/env bash

# setup dependencies
sudo ./install-dependencies.sh
sudo ./install-mget.sh
sudo ./install-dummynet.sh
sudo ./install-nginx.sh

# download files from websites
#this should not be here- to ensure reproducibility,
#we want to put the page contents on our repository
#(of course, the randomized version)
#./download.sh

rm -rf ~/scratch
cp -r scratch ~

# setup server
./configure-nginx.sh
sudo ./restart-nginx.sh

# run tests and analyze resutls
./run-mget.sh
python analyze.py

# results stored in results/
# analysis stored in table_results.csv
