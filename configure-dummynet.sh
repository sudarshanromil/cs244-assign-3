# args $1 - half of RTT

#Set MTU to 1500 (default MTU for dummynet is 16000)
sudo ifconfig lo mtu 1500

#Remove all rules
sudo ~/ipfw3-2012/ipfw/ipfw flush -f

#Add rule for uplink traffic (from client to server)
sudo ~/ipfw3-2012/ipfw/ipfw add pipe 1 tcp from any to me 8000
sudo ~/ipfw3-2012/ipfw/ipfw pipe 1 config bw 256Kbit/s delay $1 queue 30k

#Add rule for downlink traffic (from server to client)
sudo ~/ipfw3-2012/ipfw/ipfw add pipe 2 tcp from me 8000 to any
sudo ~/ipfw3-2012/ipfw/ipfw pipe 2 config bw 4Mbit/s delay $1 queue 400k
