#!/usr/bin/env bash

dir=`pwd`

# Check for uninitialized variables
set -o nounset

ctrlc() {
    cd $dir
    exit
}
trap ctrlc SIGINT

cd /tmp

rm -rf ~/cs244-assign-3/results/
mkdir ~/cs244-assign-3/results/

function download(){
	find ~/scratch/$1 -type f|sed -e "s;^$HOME/scratch;http://localhost:8000;g"|xargs -n1000 /usr/bin/time -f "%E" -o timing ~/mget/src/mget --no-cache --num-threads=6  --delete-after -q
}

function loop(){
	download amazon
	tail -n 1 timing >> ~/cs244-assign-3/results/amazon_$1_$2

	download nytimes
	tail -n 1 timing >> ~/cs244-assign-3/results/nytimes_$1_$2

	download wsj
	tail -n 1 timing >> ~/cs244-assign-3/results/wsj_$1_$2

	download wikipedia
	tail -n 1 timing >> ~/cs244-assign-3/results/wikipedia_$1_$2
}

for j in `seq 1 4`; do
for half_rtt in 10ms 50ms 100ms
  do
    echo half_rtt $half_rtt
    ~/cs244-assign-3/configure-dummynet.sh $half_rtt

    #Enable TCP Fast Open
    ~/cs244-assign-3/on-tcp-fastopen.sh

    #Actually perform the download
    for i in `seq 1 3`
      do
        loop $half_rtt on
    done

    #Disable TCP Fast Open
    ~/cs244-assign-3/off-tcp-fastopen.sh

    #Actually perform the download
    for i in `seq 1 3`
      do
	loop $half_rtt off
    done
done
done

cd $dir
